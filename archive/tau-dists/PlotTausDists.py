import numpy as np
from scipy.stats import norm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt


# Ns for which there is data
Ns = [10,25,50,100,200]

# Track log-normal parameters vs N
mus = np.zeros((len(Ns),))
sigmas = np.zeros((len(Ns),))

# Loop over available N values
for i in range(len(Ns)):
    N = Ns[i]

    # Load data (it is in units of timesteps, dt=0.01)
    taus = np.loadtxt('taus/taus_N%d.rnom13.dat'%N)
    taus = taus / 100.

    # Plot histogram
    hist, bins = np.histogram(np.log(taus), bins=30, normed=1)
    width = 0.7 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    # plt.bar(center, hist, align='center', width=width, label='$N = %d$'%N)

    # Fit a gaussian
    (mu,sigma) = norm.fit(np.log(taus))
    fitted_gaus = mlab.normpdf(bins,mu,sigma)
    # plt.plot(bins,fitted_gaus,'r--',lw=2,label='Gaussian(%.1e,%.1e)'%(mu,sigma))

    # Tabulate fitted (mu,sigma)s
    mus[i] = mu
    sigmas[i] = sigma

    # Show
    # plt.xlabel('$\log(t_{\mathrm{trans}})$',fontsize=22)
    # plt.legend(fontsize=20)
    # plt.show()
    
    

# Plot distribution dependence on N

# Fit mu(N) power law
plt.loglog(Ns,mus)
FitLine = np.polyfit(np.log(Ns),np.log(mus),1)
xmin = 10
xmax = 200
ymin = 3.5
ymax = 10.5
fineNs = np.linspace(xmin,xmax,100)
plt.loglog(fineNs,np.exp(FitLine[1])*fineNs**FitLine[0],'k--')
plt.text(2*xmin,ymax-2,r'$\mu \,=\, %.2f\, N^{\,%.2f}$' % (np.exp(FitLine[1]),FitLine[0]),fontsize=22)
plt.xlabel('$N$',fontsize=22)
plt.ylabel('$\mu$',fontsize=22)
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
plt.show()

# Fit sigma(N) power law
plt.loglog(Ns,sigmas)
FitLine = np.polyfit(np.log(Ns),np.log(sigmas),1)
xmin = 10
xmax = 200
ymin = 1e-1
ymax = 10e-1
fineNs = np.linspace(xmin,xmax,100)
plt.loglog(fineNs,np.exp(FitLine[1])*fineNs**FitLine[0],'k--')
plt.text(3*xmin,0.5,r'$\sigma \,=\, %.2f\, N^{\,%.2f}$' % (np.exp(FitLine[1]),FitLine[0]),fontsize=22)
plt.xlabel('$N$',fontsize=22)
plt.ylabel('$\sigma$',fontsize=22)
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
plt.show()

