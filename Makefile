
# Compiler and options
# Need gnu99 standard to use popen
# Links FFTW and Math
CC = gcc
CFLAGS = -std=gnu99
LFLAGS = -lm
OPENMP = -fopenmp

###

.PHONY: clean

default: multistage-nanopores.exe

multistage-nanopores.exe: multistage-nanopores.o
	$(CC) $(CFLAGS) $(OPENMP) -o $@ $^ $(LFLAGS)

multistage-nanopores.o: multistage-nanopores.c
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:.o=.c) -o $@ $(LFLAGS)

clean:
	rm -f *.o *.exe

